import React, { useContext } from "react";
import { ASYNC_STATUSES } from "../../hooks";
import { QuestionContext, QUIZ_STATUSES } from "../../QuestionContext";
import LoadingIcon from "../../assets/loading-icon.svg";
import { computeQuizScore, unEscape } from "../../util";

const Results = () => {
  const { questions, methods, data } = useContext(QuestionContext);

  const quizResults =
    data.quizStatus === QUIZ_STATUSES.FINISHED &&
    computeQuizScore(questions, data.answers);

  const handlePlayAgainClick = () => {
    methods.restartQuiz();
  };

  return (
    <div className="flex flex-col">
      <p>
        You scored <br /> {quizResults.correct} / {quizResults.total}
      </p>
      <div className="overflow-y-scroll">
        {questions.value.results.map((question, index) => (
          <div
            className={`flex m-2 ${
              quizResults.scores[index] ? "text-green-500" : "text-red-500"
            }`}
          >
            <div class="mr-6">{quizResults.scores[index] ? "+" : "-"}</div>
            <div className="text-left">{unEscape(question.question)}</div>
          </div>
        ))}
      </div>
      <button
        onClick={handlePlayAgainClick}
        className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded"
        disabled={questions.status === ASYNC_STATUSES.PENDING}
      >
        Play Again?
        {questions.status === ASYNC_STATUSES.PENDING && (
          <img
            className="inline h-4"
            src={LoadingIcon}
            alt="Quiz loading indicator"
          />
        )}
      </button>
    </div>
  );
};

export default Results;
