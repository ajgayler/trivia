import { useContext } from "react";
import { QuestionContext } from "../../QuestionContext";
import { unEscape } from "../../util";

const Question = () => {
  const { questions, methods, data } = useContext(QuestionContext);

  const currentQuestion = questions.value?.results[data.currentQuestionIndex];

  const handleAnswerClick = (answer) => {
    if (currentQuestion.type === "boolean") {
      methods.handleAnswerSubmit(data.currentQuestionIndex, answer);
    } else if (currentQuestion.type === "multiple-choice") {
      //TODO: handle other types here eventually
    }
  };

  return (
    <div className="flex flex-1 flex-col justify-between">
      <h1 className="text-lg text-gray-700">{currentQuestion.category}</h1>

      <div className="border-blue-500 border-2 rounded p-4 mt-4">
        {unEscape(currentQuestion.question)}
      </div>
      {/* could add other renderers for different question types here once needed */}
      {currentQuestion.type === "boolean" && (
        <div className="mt-4">
          <button
            onClick={() => handleAnswerClick(true)}
            className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l"
          >
            True
          </button>
          <button
            onClick={() => handleAnswerClick(false)}
            className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r"
          >
            False
          </button>
          <p className="font-light text-sm mt-4 text-slate-400">
            {data.currentQuestionIndex + 1} of {questions.value.results.length}
          </p>
        </div>
      )}
    </div>
  );
};

export default Question;
