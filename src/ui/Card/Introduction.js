import React, { useContext } from "react";
import questionMark from "../../assets/question-mark.svg";
import { ASYNC_STATUSES } from "../../hooks";
import { QuestionContext } from "../../QuestionContext";
import LoadingIcon from "../../assets/loading-icon.svg";

const Introduction = () => {
  const { questions, methods } = useContext(QuestionContext);

  const handleQuizStart = () => {
    methods.setQuizIsStarted(true);
    questions.execute();
  };

  return (
    <div className="flex-col">
      <img
        className="mb-3 w-32 h-32 rounded-full shadow-lg mx-auto"
        src={questionMark}
        alt="Question mark icon"
      />
      <h1 className="text-lg text-gray-700">
        Welcome to the Trivia Challenge!{" "}
      </h1>
      <p className="text-xs text-gray-400 mt-4">
        You will be presented with 10 True or False questions.
      </p>
      <p className="text-xs text-gray-400 mt-4">Can you score 100%?</p>
      <button
        onClick={handleQuizStart}
        className="bg-indigo-600 h-12 px-8 py-2 mt-8 rounded-3xl text-gray-100 font-semibold uppercase tracking-wide"
        disabled={questions.status === ASYNC_STATUSES.PENDING}
      >
        Begin{" "}
        {questions.status === ASYNC_STATUSES.PENDING && (
          <img
            className="inline h-4"
            src={LoadingIcon}
            alt="Quiz loading indicator"
          />
        )}
      </button>
      {questions.status === ASYNC_STATUSES.ERROR && (
        <div
          className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
          role="alert"
        >
          <span className="block sm:inline">
            Error loading questions! Please try again.
            {/* can also output `questions.error` here (though is not the most user-friendly output) */}
          </span>
        </div>
      )}
    </div>
  );
};

export default Introduction;
