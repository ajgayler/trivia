import { useContext } from "react";
import { QuestionContext, QUIZ_STATUSES } from "../../QuestionContext";
import Introduction from "./Introduction";
import Question from "./Question";
import Results from "./Results";

const Card = (props) => {
  const { data } = useContext(QuestionContext);

  return (
    <div className="Card bg-white font-semibold text-center rounded-3xl border shadow-lg p-10 m-4 max-w-sm max-h-screen flex">
      {data.quizStatus === QUIZ_STATUSES.IDLE && <Introduction />}
      {data.quizStatus === QUIZ_STATUSES.STARTED && <Question />}
      {data.quizStatus === QUIZ_STATUSES.FINISHED && <Results />}
    </div>
  );
};

export default Card;
