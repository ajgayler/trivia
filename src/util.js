export const computeQuizScore = (questions, answers) => {
  let correct = 0;
  let scores = [];
  const { results } = questions.value;

  results.forEach((question, index) => {
    const currentAnswer = answers[index];
    if (question.type === "boolean") {
      //response data is a bit weird, could maybe normalize this in
      //a response handler
      if (
        (question.correct_answer === "True" && currentAnswer === true) ||
        (question.correct_answer === "False" && currentAnswer === false)
      ) {
        correct++;
        scores[index] = true;
      } else {
        scores[index] = false;
      }
    }
    //TODO: handle more question types here
  });

  return {
    correct,
    total: results.length,
    scores,
  };
};

//The API serves escaped strings, unescape them here
export const unEscape = (str) => {
  str = str.replace(/&quot;/g, '"');
  str = str.replace(/&#039;/g, "'");
  str = str.replace(/&amp;/g, "&");
  return str;
};
