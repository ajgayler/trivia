import { useCallback, useState, useEffect } from "react";

export const ASYNC_STATUSES = {
  IDLE: "IDLE",
  PENDING: "PENDING",
  SUCCESS: "SUCCESS",
  ERROR: "ERROR",
};

//simple async hook from https://usehooks.com/useAsync/
//TODO: add a resource fetching library like or react-async if this grows more complex
export const useAsync = (asyncFunction, immediate = true) => {
  const [status, setStatus] = useState(ASYNC_STATUSES.IDLE);
  const [value, setValue] = useState(null);
  const [error, setError] = useState(null);

  const execute = useCallback(() => {
    setStatus(ASYNC_STATUSES.PENDING);
    setValue(null);
    setError(null);

    return asyncFunction()
      .then((response) => response.json())
      .then((data) => {
        setValue(data);
        setStatus(ASYNC_STATUSES.SUCCESS);
      })
      .catch((error) => {
        setError(error);
        setStatus(ASYNC_STATUSES.ERROR);
      });
  }, [asyncFunction]);

  // Call execute if we want to fire it right away.
  // Otherwise execute can be called later, such as
  // in an onClick handler.
  useEffect(() => {
    if (immediate) {
      execute();
    }
  }, [execute, immediate]);

  return { execute, status, value, error };
};
