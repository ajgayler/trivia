import React, { useEffect, useState } from "react";
import { fetchQuestions } from "./API";
import { ASYNC_STATUSES, useAsync } from "./hooks";

const QuestionContext = React.createContext();

export const QUIZ_STATUSES = {
  IDLE: "IDLE",
  STARTED: "STARTED",
  FINISHED: "FINISHED",
};

//Could be set in the UI by the user, then you could move it to a state var
const QUESTION_AMOUNT = 10;
const DEFAULT_QUIZ_PARAMS = {
  amount: QUESTION_AMOUNT,
  difficulty: "hard",
  type: "boolean",
};

function QuestionProvider(props) {
  const [quizIsStarted, setQuizIsStarted] = useState(false);
  const [answers, setAnswers] = useState([]);
  const [quizStatus, setQuizStatus] = useState(QUIZ_STATUSES.IDLE);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(null);
  const questions = useAsync(() => fetchQuestions(DEFAULT_QUIZ_PARAMS), false);

  useEffect(() => {
    if (questions.status === ASYNC_STATUSES.SUCCESS) {
      setQuizStatus(QUIZ_STATUSES.STARTED);
      setCurrentQuestionIndex(0);
    }
  }, [questions.status]);

  const handleAnswerSubmit = (index, answer) => {
    setAnswers([
      ...answers.slice(0, index),
      answer,
      ...answers.slice(index + 1),
    ]);

    const nextQuestionIndex = index + 1;

    if (nextQuestionIndex < QUESTION_AMOUNT) {
      setCurrentQuestionIndex(nextQuestionIndex);
    } else if (nextQuestionIndex === QUESTION_AMOUNT) {
      setQuizStatus(QUIZ_STATUSES.FINISHED);
    }
  };

  const restartQuiz = () => {
    setQuizStatus(QUIZ_STATUSES.IDLE);
  };

  const providerData = {
    questions,
    methods: {
      setAnswers,
      setQuizIsStarted,
      setQuizStatus,
      handleAnswerSubmit,
      restartQuiz,
    },
    data: {
      quizIsStarted,
      answers,
      quizStatus,
      currentQuestionIndex,
    },
  };

  return (
    <QuestionContext.Provider value={providerData}>
      {props.children}
    </QuestionContext.Provider>
  );
}

export { QuestionContext, QuestionProvider };
