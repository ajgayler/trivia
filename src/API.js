//TODO: move to env variable once dev/prod is set up
const BASE_URL = "https://opentdb.com/api.php";

//TODO: change to axios if this grows more complex
export const fetchQuestions = async (params) => {
  const urlParams = new URLSearchParams(params);
  return fetch(`${BASE_URL}?${urlParams}`);
};
