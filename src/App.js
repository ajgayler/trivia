// import "./App.css";
import { QuestionProvider } from "./QuestionContext";
import Card from "./ui/Card/Card";
import "./App.css";

function App() {
  return (
    <QuestionProvider>
      <div className="App flex items-center justify-center h-screen bg-gradient-to-br from-indigo-500 to-indigo-800">
        <Card />
      </div>
    </QuestionProvider>
  );
}

export default App;
